﻿/* Created by: Matthew George
 */

using UnityEngine;
using MattRGeorge.TrainingGroundArena;

/// <summary>
/// 
/// </summary>
public class PillarManagerDriver : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebugPillarManagerDriver = false;
    private string debugScriptNamePillarManagerDriver = "PillarManagerDriver";
    #endregion

    #region Public
    [SerializeField] private GameObject player = null;
    [SerializeField] private PillarManager pillarManager = null;
    [SerializeField] private bool animate = true;
    [SerializeField] private float delay = 10;
    [SerializeField] private PMHeightmap smallerHeightmap = null;
    [SerializeField] private PMHeightmap largerHeightmap = null;
    #endregion

    #region Private
    private int raiseLowerCount = 0;
    private bool smallerHMActive = true;
    #endregion
    #endregion

    #region Functions
    #region Public

    #endregion

    #region Private
    private void SpawnPlayer()
    {
        if(player != null) Instantiate(player, transform.position, Quaternion.identity);
    }

    private void RaisePillars()
    {
        pillarManager.RaisePillars(animate);
        if(player == null) Invoke("LowerPillars", delay);
    }
    private void LowerPillars()
    {
        pillarManager.LowerPillars(animate);
        animate = !animate;
        raiseLowerCount++;
        if (player == null) Invoke("RaisePillars", delay);
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Use Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        if (isDebugPillarManagerDriver) Debug.Log(((this.GetType().Name != debugScriptNamePillarManagerDriver) ? this.GetType().Name + "->" : "") + debugScriptNamePillarManagerDriver + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintWarningDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogWarning(((this.GetType().Name != debugScriptNamePillarManagerDriver) ? this.GetType().Name + "->" : "") + debugScriptNamePillarManagerDriver + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    /// /* To make this class inheritable change function from "virtual" to "override" and un-comment the base line. */
    public virtual void PrintErrorDebugMsg(string msg)
    {
        //base.PrintDebugMsg(msg);
        Debug.LogError(((this.GetType().Name != debugScriptNamePillarManagerDriver) ? this.GetType().Name + "->" : "") + debugScriptNamePillarManagerDriver + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties

    #endregion
    #endregion

    #region Start & Update Functions
    void Awake()
    {
        PrintDebugMsg("Debugging enabled.");
        Invoke("SpawnPlayer", .1f);

        pillarManager.ApplyHeightmap((smallerHMActive) ? smallerHeightmap : largerHeightmap);
        Invoke("RaisePillars", 1);
    }
    private void Update()
    {
        if (raiseLowerCount == 4)
        {
            smallerHMActive = !smallerHMActive;
            pillarManager.ApplyHeightmap((smallerHMActive) ? smallerHeightmap : largerHeightmap, true);
            raiseLowerCount = 0;
        }
    }
    #endregion
}