﻿/* Created by musta
 * 2/16/2019 6:18:28 PM
 */

using UnityEngine;

namespace MattRGeorge.TrainingGroundArena
{
    /// <summary>
    /// A pillar controlled by the PillarManager. Can get information about this pillar.
    /// </summary>
    public class Pillar : MonoBehaviour
    {
        #region variables
        #region Public

        #endregion

        #region Properties
        public float Speed
        {
            get
            {
                return speed;
            }
            set
            {
                if (value > 0) speed = value;
            }
        }
        public byte HeightIndex
        {
            get
            {
                return heightIndex;
            }
            set
            {
                heightIndex = value;
            }
        }
        public float HeightPerIndex
        {
            get
            {
                return heightPerIndex;
            }
            set
            {
                if (value > 0)
                {

                    heightPerIndex = value;
                    UpdateTransform();
                }
            }
        }
        public Vector2 LengthWidth
        {
            get
            {
                return lengthWidth;
            }
            set
            {
                if (value.x > 0 && value.y > 0)
                {
                    lengthWidth = value;
                    UpdateTransform();
                }
            }
        }

        public bool IsSpawnpoint
        {
            get
            {
                return isSpawnpoint;
            }
            set
            {
                isSpawnpoint = value;
            }
        }
        public int SpawnpointTeamNum
        {
            get
            {
                return spawnpointTeamNum;
            }
            set
            {
                spawnpointTeamNum = (value >= 0) ? value : spawnpointTeamNum;
            }
        }
        public Vector3 TopSurface
        {
            get
            {
                return transform.position + (Vector3.up * transform.localScale.y / 2);
            }
        }

        public bool IsMoving
        {
            get
            {
                return isMovingUp || isMovingDown;
            }
        }
        #endregion

        #region private
        private bool isSpawnpoint = false;
        private int spawnpointTeamNum = -1;
        private float speed = 1;
        private float heightPerIndex = 1;
        private Vector2 lengthWidth = new Vector2(1, 1);

        private byte heightIndex = 0;
        private float floorHeight = 0;

        private float targetHeight = 0;
        private float startHeight = 0;
        private bool isMovingUp = false;
        private bool isMovingDown = false;
        #endregion
        #endregion

        #region Functions
        #region Public
        /// <summary>
        /// Raise this pillar to the pre-set height index with animation.
        /// </summary>
        /// <param name="animate">Animate the pillar's movement?</param>
        public void StartRaising(bool animate = true)
        {
            startHeight = transform.position.y;
            targetHeight = floorHeight + heightIndex * heightPerIndex;

            if (animate)
            {
                isMovingUp = true;
                isMovingDown = false;
                PrintDebugMsg_Pillar("Starting to raise pillar to " + targetHeight + "(" + heightIndex + ") from " + startHeight);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, targetHeight, transform.position.z);
                PrintDebugMsg_Pillar("Raised pillar to " + targetHeight + "(" + heightIndex + ") from " + startHeight);
            }
        }
        /// <summary>
        /// Lower this pillar to the floor height index with animation.
        /// </summary>
        /// <param name="animate">Animate the pillar's movement?</param>
        public void StartLowering(bool animate = true)
        {
            startHeight = transform.position.y;
            targetHeight = floorHeight;

            if (animate)
            {
                isMovingUp = false;
                isMovingDown = true;
                PrintDebugMsg_Pillar("Starting to lower pillar to " + targetHeight + "(" + heightIndex + ") from " + startHeight);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, floorHeight, transform.position.z);
                PrintDebugMsg_Pillar("Lowered pillar to " + targetHeight + "(" + heightIndex + ") from " + startHeight);
            }
        }

        /// <summary>
        /// Update the transform of this pillar.
        /// </summary>
        public void UpdateTransform()
        {
            // Stretch our full height and make sure we are the proper x and z
            transform.localScale = new Vector3(lengthWidth.x, heightPerIndex * 255, lengthWidth.y);
            // Set the lowest position
            floorHeight = transform.position.y - transform.localScale.y / 2;
        }
        #endregion

        #region Private
        /// <summary>
        /// Move the pillar in the required direction via isMoving flags.
        /// </summary>
        private void Move()
        {
            if (isMovingUp)
            {
                transform.Translate(transform.up * speed * Time.deltaTime);
                if (transform.position.y >= targetHeight)
                {
                    PrintDebugMsg_Pillar("Reached target height.");
                    transform.position = new Vector3(transform.position.x, targetHeight, transform.position.z);
                    isMovingUp = false;
                }
            }
            else if (isMovingDown)
            {
                transform.Translate(-transform.up * speed * Time.deltaTime);
                if (transform.position.y <= targetHeight)
                {
                    PrintDebugMsg_Pillar("Reached target height.");
                    transform.position = new Vector3(transform.position.x, targetHeight, transform.position.z);
                    isMovingDown = false;
                }
            }
        }
        #endregion
        #endregion

        #region Unity Functions
        private void Awake()
        {
            PrintDebugMsg_Pillar("Debugging enabled.");
        }
        private void Update()
        {
            Move();
        }
        #endregion

        #region Template
        #region Variables
        [SerializeField] private bool isDebug_Pillar = false;
        private string isDebugScriptName_Pillar = "Pillar";
        #endregion

        #region Functions
        /// <summary>
        /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
        /// </summary>
        /// <param name="msg">The message.</param>
        private void PrintDebugMsg_Pillar(string msg)
        {
            if (isDebug_Pillar) Debug.Log(isDebugScriptName_Pillar + " (" + this.gameObject.name + "): " + msg);
        }
        /// <summary>
        /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
        /// </summary>
        /// <param name="msg">The message.</param>
        private void PrintWarningDebugMsg_Pillar(string msg)
        {
            Debug.LogWarning(isDebugScriptName_Pillar + " (" + this.gameObject.name + "): " + msg);
        }
        /// <summary>
        /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
        /// </summary>
        /// <param name="msg">The message.</param>
        private void PrintErrorDebugMsg_Pillar(string msg)
        {
            Debug.LogError(isDebugScriptName_Pillar + " (" + this.gameObject.name + "): " + msg);
        }
        #endregion
        #endregion
    }
}