﻿/* Created by musta
 * 2/16/2019 6:17:46 PM
 */

using UnityEngine;
using System;
using System.Collections.Generic;

namespace MattRGeorge.TrainingGroundArena
{
    /// <summary>
    /// The class that manages all the pillars in the room. Applies a heightmap with an optional spawnmap to each of the pillars.
    /// </summary>
    public class PillarManager : MonoBehaviour
    {
        #region variables
        #region Public
        [SerializeField] private GameObject pillarPrefab = null;
        
        [Tooltip("Starting number of pillars x and z.")]
        [SerializeField] private Vector2 startingFloorResolution = new Vector2(10, 10);
        
        [Tooltip("The movement speed of the pillars.")]
        [SerializeField] private float pillarSpeed = 1;
        [Tooltip("The amount along the y-axis for each height index (0-255) by the grayscale pixel they represent.")] // Fixed: (Must also currently be set to pillar prefab manually (pillarHeightIndex * 255 = pillarHeight.y))
        [SerializeField] private float pillarHeightPerIndex = .05f;
        [Tooltip("The length and width of the the pillars.")]
        [SerializeField] private Vector2 pillarLengthWidth = new Vector2(1, 1);
        #endregion

        #region Properties
        /// <summary>
        /// Gets all the team's spawn locations.
        /// </summary>
        public Dictionary<int, List<Vector3>> Spawnpoints
        {
            get
            {
                Dictionary<int, List<Vector3>> spawnpointsList = new Dictionary<int, List<Vector3>>();

                foreach (Transform pillar in pillars)
                {
                    Pillar pillarComp = pillar.GetComponent<Pillar>();
                    if (pillarComp != null)
                    {
                        if (pillarComp.IsSpawnpoint)
                        {
                            if(!spawnpointsList.ContainsKey(pillarComp.SpawnpointTeamNum)) spawnpointsList.Add(pillarComp.SpawnpointTeamNum, new List<Vector3>());
                            spawnpointsList[pillarComp.SpawnpointTeamNum].Add(pillarComp.TopSurface);
                        }
                    }
                    else PrintErrorDebugMsg_PillarManager("A pillar doesn't have its Pillar component!");
                }

                if (spawnpointsList.Count == 0) PrintDebugMsg_PillarManager("No spawnpoints found.");
                return spawnpointsList;
            }
        }
        /// <summary>
        /// Checks to see if pillars are moving.
        /// </summary>
        public bool ArePillarsMoving
        {
            get
            {
                foreach (Transform pillar in pillars)
                {
                    Pillar pillarComp = pillar.GetComponent<Pillar>();
                    if (pillarComp != null)
                    {
                        if (pillarComp.IsMoving) return true;
                    }
                    else PrintErrorDebugMsg_PillarManager("A pillar doesn't have its Pillar component!");
                }

                return false;
            }
        }
        #endregion

        #region private
        private Transform pillarAnchor = null;
        private List<Transform> pillars = new List<Transform>();
        private Transform unusedPillarAnchor = null;
        private List<Transform> unUsedPillars = new List<Transform>();

        private PMHeightmap currHeightmap = null;

        private Color pillarsStartingColor = new Color();
        private bool startingColorSet = false;
        #endregion
        #endregion

        #region Functions
        #region Public
        /// <summary>
        /// Apply the given heightmap with spawnmap (optional) to the pillars.
        /// </summary>
        /// <param name="heightmap">The object that has the heightmap and spawnmap.</param>
        /// <param name="updateSize">Update the sie of the room and respawn pillars if given heightmap resolution doesn't match current floor resolution.</param>
        public void ApplyHeightmap(PMHeightmap heightmap, bool updateSize = false)
        {
            if(heightmap != null && heightmap.Heightmap != null)
            {
                if (updateSize && (heightmap.Heightmap.width != startingFloorResolution.x || heightmap.Heightmap.height != startingFloorResolution.y)) SpawnPillars(heightmap.Heightmap.width, heightmap.Heightmap.height);
                else if (heightmap.Heightmap.width != startingFloorResolution.x || heightmap.Heightmap.height != startingFloorResolution.y) return;

                currHeightmap = heightmap;
                Texture2D heightmap2D = (Texture2D)currHeightmap.Heightmap;

                // Pillars are created by width(x) then rows(y), incrementing both positive directions
                float currPixelGrayscale = 0;
                int currPillarI = 0;
                int currTeamCount = 0;
                Dictionary<Color, int> knownTeams = new Dictionary<Color, int>();
                for (int i = 0; i < currHeightmap.Heightmap.height; i++)
                {
                    for (int ii = 0; ii < currHeightmap.Heightmap.width; ii++)
                    {
                        // Convert pixels to grayscale
                        currPixelGrayscale = heightmap2D.GetPixel(ii, i).grayscale;

                        // Grayscale is a percentage from 0-1, need inverse of 255 (255 pillar height index represents black)
                        Pillar currPillarComp = pillars[currPillarI].GetComponent<Pillar>();
                        currPillarComp.HeightIndex = (byte)Mathf.Abs((currPixelGrayscale * 255f) - 255f);
                        PrintDebugMsg_PillarManager("Pixel (" + i + ", " + ii + ") grayscale: " + currPixelGrayscale + " (" + heightmap2D.GetPixel(ii, i) + ") | Resulting height index: " + pillars[currPillarI].GetComponent<Pillar>().HeightIndex);

                        // Mark this pillar a spawnpoint if it is on the spawnmap
                        Texture2D spawnmap2D = (currHeightmap.Spawnmap != null) ? (Texture2D)currHeightmap.Spawnmap : null;
                        if (spawnmap2D != null && spawnmap2D.width == heightmap2D.width && spawnmap2D.height == heightmap2D.height && spawnmap2D.GetPixel(ii, i).a != 0)
                        {
                            if (!knownTeams.ContainsKey(spawnmap2D.GetPixel(ii, i)))
                            {
                                knownTeams.Add(spawnmap2D.GetPixel(ii, i), currTeamCount);
                                currTeamCount++;
                            }

                            currPillarComp.IsSpawnpoint = true;
                            currPillarComp.SpawnpointTeamNum = knownTeams[spawnmap2D.GetPixel(ii, i)];
                            if (isDebug_PillarManager)
                            {
                                if (!startingColorSet)
                                {
                                    pillarsStartingColor = currPillarComp.GetComponent<MeshRenderer>().material.color;
                                    startingColorSet = true;
                                }
                                currPillarComp.GetComponent<MeshRenderer>().material.color = spawnmap2D.GetPixel(ii, i);
                            }
                            PrintDebugMsg_PillarManager("\tPillar (" + i + ", " + ii + ") is a spawnpoint for team " + currPillarComp.SpawnpointTeamNum + ".");
                        }
                        else
                        {
                            currPillarComp.IsSpawnpoint = false;
                            if (isDebug_PillarManager && startingColorSet) currPillarComp.GetComponent<MeshRenderer>().material.color = pillarsStartingColor;
                        }

                        currPillarI++;
                    }
                }
            }
        }

        /// <summary>
        /// Raise all the pillars to thier height index.
        /// </summary>
        /// <param name="animate">Animate the pillars' movement?</param>
        public void RaisePillars(bool animate = true)
        {
            foreach (Transform pillar in pillars) pillar.GetComponent<Pillar>().StartRaising(animate);
        }
        /// <summary>
        /// Lower all the pillars to 0 height index.
        /// </summary>
        /// <param name="animate">Animate the pillars' movement?</param>
        public void LowerPillars(bool animate = true)
        {
            foreach (Transform pillar in pillars) pillar.GetComponent<Pillar>().StartLowering(animate);
        }
        #endregion

        #region Private
        /// <summary>
        /// Initial setup.
        /// </summary>
        private void SetUp()
        {
            if (pillarPrefab == null) PrintErrorDebugMsg_PillarManager("No pillar prefab assigned!");
        }

        /// <summary>
        /// Spawns a group of pillars with the default length and width being pillar count. Creates width then moves to next row(length), both positive directions.
        /// </summary>
        private void SpawnPillars()
        {
            SpawnPillars((int)startingFloorResolution[0], (int)startingFloorResolution[1]);
        }
        /// <summary>
        /// Spawns a group of pillars with the given length and width being pillar count. Creates width then moves to next row(length), both positive directions.
        /// </summary>
        /// <param name="length">How many pillars along z.</param>
        /// <param name="width">How many pillars along x. </param>
        private void SpawnPillars(int length, int width)
        {
            if(pillarPrefab != null)
            {
                if (length < 1 || width < 1) return;
                startingFloorResolution.x = length;
                startingFloorResolution.y = width;

                // Remove previesly used pillars.
                if (pillars.Count > 0) RemovePillars();

                PrintDebugMsg_PillarManager("Spawning pillars...");
                if (pillarAnchor == null)
                {
                    pillarAnchor = new GameObject("PILLAR_ANCHOR").transform;
                    pillarAnchor.parent = transform;
                }

                // Add pillars starting along the x and then move on to the next row along z
                Vector3 floorPos = new Vector3(transform.position.x + pillarPrefab.transform.localScale.x / 2, transform.position.y, transform.position.z + pillarPrefab.transform.localScale.z / 2);
                Transform currRowAnchor = null;
                Transform currPillar = null;
                Pillar currPillarComp = null;
                for (int i = 0; i < length; i++)
                {
                    // Re-use same row anchors from last time if any
                    if (pillarAnchor.childCount > 0 && pillarAnchor.childCount > i) currRowAnchor = pillarAnchor.GetChild(i);
                    else
                    {
                        currRowAnchor = new GameObject("ROW_" + i).transform;
                        currRowAnchor.parent = pillarAnchor;
                    }

                    for (int ii = 0; ii < width; ii++)
                    {
                        // Re-use pillars if any
                        if (unUsedPillars.Count > 0)
                        {
                            currPillar = ReUsePillar();
                            currPillar.transform.position = floorPos;
                        }
                        else currPillar = Instantiate(pillarPrefab, floorPos, Quaternion.identity).transform;

                        // Position pillar and append " row-pillar" to the name
                        if (currPillar.GetComponent<Pillar>() == null) currPillarComp = currPillar.gameObject.AddComponent<Pillar>();
                        else currPillarComp = currPillar.GetComponent<Pillar>();
                        
                        currPillarComp.Speed = pillarSpeed;
                        currPillarComp.HeightPerIndex = pillarHeightPerIndex;
                        currPillarComp.LengthWidth = pillarLengthWidth;
                        currPillarComp.HeightIndex = 0;
                        float pillarPosX = currPillar.localScale.x * ii;
                        float pillarPosZ = currPillar.localScale.z * i;
                        currPillar.Translate(new Vector3(pillarPosX, currPillar.position.y, pillarPosZ));
                        currPillar.name += " " + i + "-" + ii;
                        currPillar.parent = currRowAnchor;
                        pillars.Add(currPillar);
                    }
                }

                PrintDebugMsg_PillarManager("\tCreated " + pillars.Count + " pillar(s).");
                LowerPillars(false);
                if(currHeightmap != null)
                {
                    if (currHeightmap.Heightmap.width != startingFloorResolution.x || currHeightmap.Heightmap.height != startingFloorResolution.y) currHeightmap = null;
                    else ApplyHeightmap(currHeightmap);
                }
            }
        }

        /// <summary>
        /// Re-use a pillar that is not in use.
        /// </summary>
        /// <returns>The pillar to be re-used.</returns>
        private Transform ReUsePillar()
        {
            PrintDebugMsg_PillarManager("Re-using a pillar...");

            // Re-use last pillar
            Transform pillar = unUsedPillars[unUsedPillars.Count - 1];
            unUsedPillars.Remove(pillar);
            pillar.gameObject.SetActive(true);

            // Reset name
            string[] currName = pillar.name.Split(' ');
            pillar.name = "";
            for (int i = 0; i < currName.Length - 1; i++) pillar.name += currName[i];

            PrintDebugMsg_PillarManager("\tUnused pillars: " + unUsedPillars.Count + " | Current pillars: " + pillars.Count);
            return pillar;
        }
        /// <summary>
        /// Remove all pillars that are currently in use and adds them to the un-used pillars list.
        /// </summary>
        private void RemovePillars()
        {
            PrintDebugMsg_PillarManager("Removing current pillars...");

            if (unusedPillarAnchor == null)
            {
                unusedPillarAnchor = new GameObject("UNUSED_PILLAR_ANCHOR").transform;
                unusedPillarAnchor.parent = transform;
            }

            // Add the pillars to the un-used pillars list and clear the currently used pillars list
            foreach(Transform pillar in pillars)
            {
                unUsedPillars.Add(pillar);
                pillar.parent = unusedPillarAnchor;
                pillar.gameObject.SetActive(false);
            }
            pillars = new List<Transform>();

            PrintDebugMsg_PillarManager("\tUnused pillars: " + unUsedPillars.Count + " | Current pillars: " + pillars.Count);
        }
        #endregion
        #endregion

        #region Unity Functions
        private void Awake()
        {
            PrintDebugMsg_PillarManager("Debugging enabled.");

            SetUp();
            SpawnPillars();
        }
        #endregion

        #region Template
        #region Variables
        [SerializeField] private bool isDebug_PillarManager = false;
        private string isDebugScriptName_PillarManager = "PillarManager";
        #endregion

        #region Functions
        /// <summary>
        /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
        /// </summary>
        /// <param name="msg">The message.</param>
        private void PrintDebugMsg_PillarManager(string msg)
        {
            if (isDebug_PillarManager) Debug.Log(isDebugScriptName_PillarManager + " (" + this.gameObject.name + "): " + msg);
        }
        /// <summary>
        /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
        /// </summary>
        /// <param name="msg">The message.</param>
        private void PrintWarningDebugMsg_PillarManager(string msg)
        {
            Debug.LogWarning(isDebugScriptName_PillarManager + " (" + this.gameObject.name + "): " + msg);
        }
        /// <summary>
        /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
        /// </summary>
        /// <param name="msg">The message.</param>
        private void PrintErrorDebugMsg_PillarManager(string msg)
        {
            Debug.LogError(isDebugScriptName_PillarManager + " (" + this.gameObject.name + "): " + msg);
        }
        #endregion
        #endregion
    }

    /// <summary>
    /// A helper class intended to store the information about a PillarManager Heightmap.
    /// </summary>
    [Serializable]
    public class PMHeightmap
    {
        [SerializeField] private Texture heightmap = null;
        [SerializeField] private Texture spawnmap = null;

        public Texture Heightmap
        {
            get
            {
                return heightmap;
            }
            set
            {
                heightmap = (heightmap.width == value.width && heightmap.height == value.height) ? value : heightmap;
            }
        }
        public Texture Spawnmap
        {
            get
            {
                return spawnmap;
            }
            set
            {
                spawnmap = (spawnmap.width == value.width && spawnmap.height == value.height) ? value : spawnmap;
            }
        }
    }
}